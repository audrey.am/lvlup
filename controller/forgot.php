<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require '..\vendor\phpmailer\phpmailer\src\PHPMailer.php';
require '..\vendor\phpmailer\phpmailer\src\SMTP.php';
require '../vendor\phpmailer\phpmailer\src\Exception.php';
require '../vendor/autoload.php';
require_once __DIR__ . '/../model/config.php';

//Load Composer's autoloader



if (!empty($_POST['email'])) {
    $email = htmlspecialchars($_POST['email']);

    $check = $bdd->prepare('SELECT usedTokens FROM wp_amelia_users WHERE email = ?');
    $check->execute(array($email));
    $data = $check->fetch();
    $row = $check->rowCount();
    if ($row) {
        $token = bin2hex(openssl_random_pseudo_bytes(24));
        $token_user = $data['usedTokens']; // attention longueur du token : 128, prevoyez un varchar 130 dans votre table si vous utilisez les tokens du système d'inscription
        if ($token_user === NULL) {
            $token_user = bin2hex(openssl_random_pseudo_bytes(64));
            $update = $bdd->prepare('UPDATE wp_amelia_users SET `usedTokens`=:newToken WHERE `email`=:email ');
            $update->execute(array(
                'newToken' => $token_user,
                'email' => $email,
            ));
        }

        $insert = $bdd->prepare('INSERT INTO wp_password_recover(token_user, token) VALUES(?,?)');
        $insert->execute(array($token_user, $token));
        ini_set('display_errors', 1);
        error_reporting(E_ALL);
        $link = 'http://localhost/lvlup/controller/recover.php?u=' . base64_encode($token_user) . '&token=' . base64_encode($token);


        $mail = new PHPMailer(true);

        try {
            //Server settings
            $mail->SMTPDebug = SMTP::DEBUG_SERVER;                      //Enable verbose debug output
            $mail->isSMTP();                                            //Send using SMTP
            $mail->Host       = 'ssl0.ovh.net';                     //Set the SMTP server to send through
            $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
            $mail->Username   = 'support@leocanet.fr';                     //SMTP username
            $mail->Password   = '41y3rqJ%CCe1';                               //SMTP password
            $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;            //Enable implicit TLS encryption
            $mail->Port       = 587;                                    //TCP port to connect to; use 587 if you have set `SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS`

            //Recipients
            $mail->setFrom('support@leocanet.fr', 'Mailer');
            $mail->addAddress($email);     //Add a recipient

            //Content
            $mail->isHTML(true);                                  //Set email format to HTML
            $mail->Subject = 'mot de passe oublié?';
            $mail->Body    = "Cliquez sur ce " . "<a href=$link>lien</a> pour changer de mot de passe";

            $mail->send();
            echo 'Message has been sent';
            header('Location:../view/index.php?mail=ok');
        } catch (Exception $e) {
            echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
            header('Location:../view/index.php?mail=ko');
        }
    } else {
        echo "Compte non existant";
        header('Location:../view/index.php?mail=mailinconnu');
        #header('Location: ../index.php');
        #die();
    }
}
