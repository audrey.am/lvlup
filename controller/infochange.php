<?php
session_start();
require_once '../model/config.php';
require '../model/data.php';
if (!isset($_SESSION['user'])) {
    header('Location:./view/index.php');
    die();
}
$req = $bdd->prepare('SELECT * FROM wp_amelia_users WHERE usedTokens = ?');
$req->execute(array($_SESSION['user']));
$data = $req->fetch();
// Je stocke l'id du client
$customerId = $data['id'];
$customerEventData = getAllCustomersEventsDatas($customerId);

if (!empty($_POST['phone']) && !empty($_POST['adress']) && !empty($_POST['zipcode']) && !empty($_POST['city'])) {
    // Patch XSS

    $phone = htmlspecialchars($_POST['phone']);
    $adress = htmlspecialchars($_POST['adress']);
    $zipcode = htmlspecialchars($_POST['zipcode']);
    $city = htmlspecialchars($_POST['city']);
    $societe = htmlspecialchars($_POST['societe']);
    $tva = htmlspecialchars($_POST['tva']);
    $email = strtolower($email); // on transforme toutes les lettres majuscules en minuscules pour éviter que Foo@gmail.com et foo@gmail.com soient deux comptes différents ..



    updateInfoFromOneClient($phone, $adress, $zipcode, $city, $societe, $tva, $customerId);
    header('Location: ../view/landing.php');
} else {
    echo 'bug';
}
