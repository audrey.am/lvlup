<?php
require_once '../model/config.php'; // On inclu la connexion à la bdd

// Si les variables existent et qu'elles ne sont pas vides
if (!empty($_POST['firstName']) && !empty($_POST['lastName']) && !empty($_POST['email']) && !empty($_POST['birthday']) && !empty($_POST['password']) && !empty($_POST['password_retype']) && !empty($_POST['phone']) && !empty($_POST['adress']) && !empty($_POST['zipcode']) && !empty($_POST['city'])) {
    // Patch XSS
    $firstName = htmlspecialchars($_POST['firstName']);
    $lastName = htmlspecialchars($_POST['lastName']);
    $email = htmlspecialchars($_POST['email']);
    $birthday = htmlspecialchars($_POST['birthday']);
    $password = htmlspecialchars($_POST['password']);
    $password_retype = htmlspecialchars($_POST['password_retype']);
    $phone = htmlspecialchars($_POST['phone']);
    $adress = htmlspecialchars($_POST['adress']);
    $zipcode = htmlspecialchars($_POST['zipcode']);
    $city = htmlspecialchars($_POST['city']);
    $societe = htmlspecialchars($_POST['societe']);
    $tva = htmlspecialchars($_POST['tva']);
    // On vérifie si l'utilisateur existe
    $check = $bdd->prepare('SELECT firstName, lastName, email FROM wp_amelia_users WHERE email = ?');
    $check->execute(array($email));
    $data = $check->fetch();
    $row = $check->rowCount();

    $email = strtolower($email); // on transforme toute les lettres majuscule en minuscule pour éviter que Foo@gmail.com et foo@gmail.com soient deux compte différents ..

    // Si la requete renvoie un 0 alors l'utilisateur n'existe pas 
    if ($row == 0) {
        if (strlen($firstName) <= 100 && strlen($lastName) <= 100) { // On verifie que la longueur du firstName <= 100
            if (strlen($email) <= 100) { // On verifie que la longueur du mail <= 100
                if (filter_var($email, FILTER_VALIDATE_EMAIL)) { // Si l'email est de la bonne forme
                    if ($password === $password_retype) { // si les deux mdp saisis sont bon

                        // On hash le mot de passe avec Bcrypt, via un coût de 12
                        $cost = ['cost' => 12];
                        $password = password_hash($password, PASSWORD_BCRYPT, $cost);


                        // On insère dans la base de données
                        $insert = $bdd->prepare('INSERT INTO wp_amelia_users (firstName, lastName, email, birthday, password, phone, adress, zipcode, city, societe, tva, usedTokens) VALUES(:firstName, :lastName, :email, :birthday, :password, :phone,:adress, :zipcode, :city, :societe, :tva,  :usedTokens)');
                        $insert->execute(array(
                            'firstName' => $firstName,
                            'lastName' => $lastName,
                            'email' => $email,
                            'birthday' => $birthday,
                            'password' => $password,
                            'phone' => $phone,
                            'adress' => $adress,
                            'zipcode' => $zipcode,
                            'city' => $city,
                            'societe' => $societe,
                            'tva' => $tva,
                            'usedTokens' => bin2hex(openssl_random_pseudo_bytes(64))
                        ));
                        // On redirige avec le message de succès
                        header('Location:../view/inscription.php?reg_err=success');
                        die();
                    } else {
                        header('Location: ../view/inscription.php?reg_err=password');
                        die();
                    }
                } else {
                    header('Location: ../view/inscription.php?reg_err=email');
                    die();
                }
            } else {
                header('Location: ../view/inscription.php?reg_err=email_length');
                die();
            }
        } else {
            header('Location: ../view/inscription.php?reg_err=pseudo_length');
            die();
        }
    } else {
        header('Location: ../view/inscription.php?reg_err=already');
        die();
    }
}
