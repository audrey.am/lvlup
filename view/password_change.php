<?php
require_once __DIR__ . '/../model/config.php';
if (!empty($_GET['u'])) {
  $token = htmlspecialchars(base64_decode($_GET['u']));
  $check = $bdd->prepare('SELECT * FROM wp_password_recover WHERE token_user = ?');
  $check->execute(array($token));
  $row = $check->rowCount();

  if ($row == 0) {
    echo "Lien non valide";
    die();
  }
}
?>
<!doctype html>
<html lang="fr">

<head>
  <title>Réinitialiser mon mot de passe</title>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


  <link rel="stylesheet" href="styles-landing.css">
</head>

<body>
  <div class="container">
    <div class="card-body" style="display:block; height: 40vh; width:40vw">
      <h4 class="titre" style="margin-top:0">Réinitialiser mon mot de passe</h4>
      <div class="form-group">
        <form action="../controller/password_change_post.php" method="POST">
          <input type="hidden" name="token" value="<?php echo base64_decode(htmlspecialchars($_GET['u'])); ?>" />
          <div class="form__group field">
            <input type="password" name="password" class="form__field" required />
            <label for="password" class="form__label">Mot de passe</label>
          </div>
          <div class="form__group field">
            <input type="password" name="password_repeat" class="form__field" required />
            <label for="password_repeat" class="form__label">Retapez le mot de passe</label>
          </div>
          <div class="form__group field">
            <button type="submit" class="btn-5">Modifier</button>
          </div>
        </form>
      </div>
    </div>
  </div>



</body>

</html>