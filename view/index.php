<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <link rel="stylesheet" href="style.css">
    <title>Connexion</title>
</head>

<body>
    <div class="container">
        <div class="col-md-12">
            <?php
            if (isset($_GET['mail'])) {
                $mail = htmlspecialchars($_GET['mail']);

                switch ($mail) {
                    case 'ok':
            ?>
                        <div class="alert green">
                            <p>Un mail vous a été envoyé</p>
                        </div>
                    <?php
                        break;

                    case 'ko':
                    ?>
                        <div class="alert red">
                            <p> une erreur est survenue</p>
                        </div>
                    <?php
                        break;
                    case 'mailinconnu':
                    ?>
                        <div class="alert orange">
                            <p>L'email n'existe pas, n'hesitez pas à vous inscrire avec ce <a href="http://localhost/lvlup/view/inscription.php">lien</a></p>
                        </div>
            <?php
                        break;
                }
            }
            ?>
            <div class="login-form">
                <?php
                if (isset($_GET['login_err'])) {
                    $err = htmlspecialchars($_GET['login_err']);

                    switch ($err) {
                        case 'password':
                ?>
                            <div class="alert alert-danger">
                                <strong>Erreur</strong> mot de passe incorrect
                            </div>
                        <?php
                            break;

                        case 'email':
                        ?>
                            <div class="alert alert-danger">
                                <strong>Erreur</strong> email incorrect
                            </div>
                        <?php
                            break;

                        case 'already':
                        ?>
                            <div class="alert alert-danger">
                                <strong>Erreur</strong> compte non existant
                            </div>
                <?php
                            break;
                    }
                }
                ?>

                <form action="../controller/connexion.php" method="post">
                    <h2 class="text-center">Connexion</h2>
                    <div class="form__group field" style="width:100%">
                        <input type="email" name="email" class="form__field" required="required" autocomplete="off">
                        <label for="phone" class="form__label">Email</label>
                    </div>
                    <div class="form__group field">
                        <input type="password" name="password" class="form__field" required="required" autocomplete="off">
                        <label for="password" class="form__label">Mot de passe</label>
                    </div>
                    <div class="form__group field">
                        <button type="submit" class="btn-5">Connexion</button>
                    </div>
                </form>
                <p class="text-center pDansA"><a href="../view/inscription.php">Inscription</a></p>
                <p class="text-center pDansA"><a href="../view/motdepasseoublie.php">Mot de passe oublié?</a></p>
            </div>
            <style>
                .login-form {
                    width: 340px;
                    margin: 50px auto;
                }

                .login-form form {
                    margin-bottom: 15px;
                    background: #f7f7f7;
                    box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
                    padding: 30px;
                }

                .login-form h2 {
                    margin: 0 0 15px;
                }
            </style>
</body>

</html>