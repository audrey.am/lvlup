<?php
session_start();
require_once '../model/config.php'; // ajout connexion bdd
require '../model/data.php';
// si la session existe pas soit si l'on est pas connecté on redirige
if (!isset($_SESSION['user'])) {
    header('Location:./view/index.php');
    die();
}

// On récupere les données de l'utilisateur
$req = $bdd->prepare('SELECT * FROM wp_amelia_users WHERE usedTokens = ?');
$req->execute(array($_SESSION['user']));
$data = $req->fetch();
// Je stocke l'id du client
$customerId = $data['id'];

//Je récupère les données des events auquel l'utilisateur participe
$customerEventData = getAllCustomersEventsDatas($customerId);


?>
<!doctype html>
<html lang="en">

<head>
    <title>Espace membre</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="styles-modal.css">
    <link rel="stylesheet" href="styles-landing.css">
</head>

<body>
    <main>
        <div class="container">
            <div class="col-md-12">
                <?php
                if (isset($_GET['err'])) {
                    $err = htmlspecialchars($_GET['err']);
                    switch ($err) {
                        case 'current_password':
                            echo "<div class='alert alert-danger'>Le mot de passe actuel est incorrect</div>";
                            break;

                        case 'success_password':
                            echo "<div class='alert alert-success'>Le mot de passe a bien été modifié ! </div>";
                            break;
                    }
                }
                ?>

                <div class="touctouc">
                    <div class="customerInfo">
                        <img src="../arpasys-groupe.png">
                        <h3>Vos informations</h3>
                        <p><?= $data['firstName'] ?> <?= $data['lastName'] ?></p>
                        <p>Email: <?= $data['email'] ?></p>
                        <p>Tel: <?= $data['phone'] ?></p>
                        <p>Adresse: <?= $data['adress'] ?></p>
                        <p><?= $data['zipcode'] ?>, <?= $data['city'] ?></p>
                        <?php if ($data['societe'] == null && $data['tva'] == null) {
                            echo 'Vous n\'etes pas relié à une société';
                        } else { ?>
                            <p>Nom de la Societe: <?= $data['societe'] ?></p>
                            <p>N° TVA: <?= $data['tva'] ?></p>
                        <?php
                        } ?>

                        <!-- Trigger/Open The Modal -->
                        <button type="button" class="btn-5 " id="myBtn">Modifier mes informations</button>
                        <p class="pDansA"><a href="../controller/deconnexion.php">Déconnexion</a></p>

                        <!-- The Modal -->
                        <div id="myModal" class="modal">
                            <div class="modal-content" id="modal-content">
                                <span class="close" id="close">&times;</span>
                                <h3>Modifier mes informations</h3>
                                <form action="../controller/infochange.php" method="post">


                                    <div class="form__group">
                                        <input type="text" name="phone" class="form__field" value="<?= $data['phone'] ?>" required="required" autocomplete="off">
                                        <label for="phone" class="form__label">Téléphone</label>
                                    </div>
                                    <div class="form__group">
                                        <input type="text" name="adress" class="form__field" value="<?= $data['adress'] ?>" required="required" autocomplete="off">
                                        <label for="adress" class="form__label">Adresse</label>
                                    </div>
                                    <div class="form__group ">
                                        <input type="text" name="zipcode" class="form__field" value="<?= $data['zipcode'] ?>" required="required" autocomplete="off">
                                        <label for="zipcode" class="form__label">Code Postal</label>
                                    </div>
                                    <div class="form__group">
                                        <input type="text" name="city" class="form__field" value="<?= $data['city'] ?>" required="required" autocomplete="off">
                                        <label for="city" class="form__label">Ville</label>
                                    </div>
                                    <div class="form__group">
                                        <input type="text" name="societe" class="form__field" value="<?= $data['societe'] ?>" required="required" autocomplete="off">
                                        <label for="societe" class="form__label">Nom de la société (non requis)</label>
                                    </div>
                                    <div class="form__group">
                                        <input type="text" name="tva" class="form__field" value="<?= $data['tva'] ?>" required="required" autocomplete="off">
                                        <label for="tva" class="form__label">Numéro TVA (non requis)</label>
                                    </div>
                                    <div class="form__group">
                                        <button type="submit" class="btn-5">Valider</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="espaceClient">
                        <div class="text-center">
                            <div class='titre'>
                                <h1>Bienvenue sur votre espace client <strong>Arpasys</strong></h1>

                            </div>
                            <div class="rdv">
                                <h1 class="p-5">Bonjour <?php echo $data['firstName']; ?>
                                    <?= $data['lastName'] ?> !</h1>
                                <h3>Vos prochains rendez-vous</h3>
                                <?php if ($customerEventData == null) {
                                    echo 'Vous n\'avez pas encore réservé de cours </br> Vous pouvez toujours le faire en cliquant sur ce <a href="http://localhost/wordpress/page-d-exemple/" >lien</a>';
                                } else { ?>
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Nom de l'évènement</th>
                                                <th>Début</th>
                                                <th>Fin</th>
                                                <th>Description</th>
                                                <th>Statut</th>
                                                <th>Prix</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php

                                            foreach ($customerEventData as $dataEvent) {

                                                $dateDebut = date("d/m/Y H:i", strtotime($dataEvent['periodStart']));
                                                $dateFin = date("d/m/Y H:i", strtotime($dataEvent['periodEnd']));
                                            ?>

                                                <tr>
                                                    <td><?= $dataEvent["name"] ?></td>
                                                    <td><?= $dateDebut ?></td>
                                                    <td><?= $dateFin ?></td>
                                                    <td><?= $dataEvent["description"] ?></td>
                                                    <td><?= $dataEvent["status"] ?></td>
                                                    <td><?= $dataEvent["price"] ?></td>

                                                </tr>
                                        <?php }
                                        } ?>
                                        </tbody>
                                    </table>


                                    <div class="bouton">
                                        <button type="button" class="btn-5" id="btnchangepass" data-toggle="modal" data-target="#change_password">
                                            Changer mon mot de passe
                                        </button>

                                        <!-- Button trigger modal -->
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <!-- The Modal -->
        <div id="changePass" class="modal">

            <div class="modal-content">
                <span class="closepass">&times;</span>
                <form action="layouts/change_password.php" method="POST">
                    <div class="form__group">
                        <label for='current_password' class="form__label">Mot de passe actuel</label>
                        <input type="password" class="form__field" id="current_password" name="current_password" class="form-control" required />

                    </div><br />
                    <div class="form__group">
                        <label for='new_password' class="form__label">Nouveau mot de passe</label>
                        <input type="password" class="form__field" id="new_password" name="new_password" class="form-control" required />
                    </div><br />
                    <div class="form__group">
                        <label for='new_password_retype' class="form__label">Re tapez le nouveau mot de passe</label>
                        <input type="password" class="form__field" id="new_password_retype" name="new_password_retype" class="form-control" required />
                    </div><br />
                    <div class="form__group">
                        <button type="submit" class="btn-5 btn-success">Sauvegarder</button>
                    </div>
                </form>
            </div>

        </div>

        <main>


            <script>
                var changePass = document.getElementById("changePass");
                var modal = document.getElementById("myModal");
                var btn = document.getElementById("myBtn");
                var btnchangepass = document.getElementById("btnchangepass");
                var span = document.getElementsByClassName("close")[0];
                var spanchangepass = document.getElementsByClassName("closepass")[0];


                btn.onclick = function() {
                    modal.style.display = "block";
                }
                btnchangepass.onclick = function() {
                    changePass.style.display = "block";
                }

                span.onclick = function() {
                    modal.style.display = "none";
                    changePass.style.display = "none";
                }
                spanchangepass.onclick = function() {

                    changePass.style.display = "none";
                }
                window.onclick = function(event) {
                    if (event.target == modal) {
                        modal.style.display = "none";
                    }
                }
                window.onclick = function(event) {
                    if (event.target == changePass) {
                        changePass.style.display = "none";
                    }
                }
            </script>
</body>

</html>