<!doctype html>
<html lang="fr">

<head>
  <title>Arpasys LevelUp | Mot de passe oublié</title>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="styles-landing.css">
</head>

<body>
  <div class="container">
    <div class="card-body">
      <div class="mdp">
        <h4 class="card-title p-3">J'ai oublié mon mot de passe</h4>
        <div class="form-group">
          <form action="../controller/forgot.php" method="POST">
            <div class="form__group field">
              <input type="email" class="form__field" name="email" autocomplete="off" required />
              <label for="email" class="form__label">Email</label>
              <button type="submit" class="btn-5">Envoyer</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</body>

</html>