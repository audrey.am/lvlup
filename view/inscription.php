<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="style.css">
    <!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"> -->
    <title>Inscription</title>
</head>

<body>
    <div class="login-form">
        <?php
        if (isset($_GET['reg_err'])) {
            $err = htmlspecialchars($_GET['reg_err']);

            switch ($err) {
                case 'success':
        ?>
                    <div class="alert alert-success">
                        <strong>Succès</strong> Inscription réussie !
                    </div>
                <?php

                    header('Location: ../view/index.php');
                    die();

                case 'password':
                ?>
                    <div class="alert alert-danger">
                        <strong>Erreur</strong> Mot de passe différent
                    </div>
                <?php
                    break;

                case 'email':
                ?>
                    <div class="alert alert-danger">
                        <strong>Erreur</strong> Email non valide
                    </div>
                <?php
                    break;

                case 'email_length':
                ?>
                    <div class="alert alert-danger">
                        <strong>Erreur</strong> Email trop long
                    </div>
                <?php
                    break;

                case 'pseudo_length':
                ?>
                    <div class="alert alert-danger">
                        <strong>Erreur</strong> Nom ou prénom trop long
                    </div>
                <?php
                case 'already':
                ?>
                    <div class="alert alert-danger">
                        <strong>Erreur</strong> Compte deja existant
                    </div>
        <?php

            }
        }
        ?>

        <form action="../controller/inscription_traitement.php" method="post">
            <h2 class="text-center">Inscription</h2>
            <div class="form__group field">
                <input type="text" name="firstName" class="form__field" required="required" autocomplete="off">
                <label for="name" class="form__label">Prénom</label>
            </div>
            <div class="form__group field">
                <input type="text" name="lastName" class="form__field" required="required" autocomplete="off">
                <label for="lastName" class="form__label">Nom de famille</label>
            </div>
            <div class="form__group field">
                <input type="email" name="email" class="form__field" required="required" autocomplete="off">
                <label for="email" class="form__label">Email</label>
            </div>
            <div class="form__group field">
                <input type="date" name="birthday" class="form__field" required="required" autocomplete="off">
                <label for="birthday" class="form__label">Date de naissance</label>
            </div>

            <div class="form__group field">
                <input type="text" name="phone" class="form__field" required="required" autocomplete="off">
                <label for="phone" class="form__label">Téléphone</label>
            </div>
            <div class="form__group field">
                <input type="text" name="adress" class="form__field" required="required" autocomplete="off">
                <label for="adress" class="form__label">Adresse</label>
            </div>
            <div class="form__group field">
                <input type="text" name="zipcode" class="form__field" required="required" autocomplete="off">
                <label for="zipcode" class="form__label">Code postal</label>
            </div>
            <div class="form__group field">
                <input type="text" name="city" class="form__field" required="required" autocomplete="off">
                <label for="city" class="form__label">Ville</label>
            </div>

            <div class="form__group field">
                <input type="password" name="password" class="form__field" required="required" autocomplete="off">
                <label for="name" class="form__label">Mot de passe</label>
            </div>
            <div class="form__group field">
                <input type="password" name="password_retype" class="form__field" required="required" autocomplete="off">
                <label for="password" class="form__label">Re-tapez le mot de passe</label>
            </div>
            <p>Vous appartenez à une société? </p>
            <div class="form__group field">
                <input type="text" name="societe" class="form__field" autocomplete="off">
                <label for="societe" class="form__label">Société (non requis)</label>
            </div>
            <div class="form__group field">
                <input type="text" name="tva" class="form__field" autocomplete="off">
                <label for="tva" class="form__label">Numéro Tva (non requis)</label>
            </div>
            <br>
            <hr>
            <br>
            <div class="form__group field">
                <button type="submit" class="btn-5">Inscription</button>
            </div>
        </form>
    </div>
    <style>
        .login-form {
            width: 50%;
            min-width: 370px;
            margin: 50px auto;
        }

        .login-form form {
            margin-bottom: 15px;
            background: #f7f7f7;
            box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
            padding: 30px;
        }

        .login-form h2 {
            margin: 0 0 15px;
        }

        .form__field,
        .btn {
            min-height: 38px;
            border-radius: 2px;
        }

        .btn {
            font-size: 15px;
            font-weight: bold;
        }
    </style>
</body>

</html>