<?php require_once '../model/config.php';

function getAllCustomersEventsDatas($customerId)
{
   global $bdd;
   $req = $bdd->prepare('SELECT 
    
    wp_amelia_customer_bookings.price, 
    wp_amelia_customer_bookings.persons, 
    wp_amelia_customer_bookings.status, 
    wp_amelia_events_periods.periodStart, 
    wp_amelia_events_periods.periodEnd, 
    wp_amelia_events.name, 
    wp_amelia_events.description, 
    wp_amelia_users.firstName, 
    wp_amelia_users.lastName FROM `wp_amelia_customer_bookings` 
    INNER JOIN `wp_amelia_customer_bookings_to_events_periods` 
    ON wp_amelia_customer_bookings.id = wp_amelia_customer_bookings_to_events_periods.customerBookingId 
    INNER JOIN `wp_amelia_events` 
    ON wp_amelia_customer_bookings_to_events_periods.eventPeriodId = wp_amelia_events.id 
    INNER JOIN `wp_amelia_events_periods` 
    ON wp_amelia_events.id = wp_amelia_events_periods.id 
    INNER JOIN `wp_amelia_events_to_providers` 
    ON wp_amelia_events_periods.id = wp_amelia_events_to_providers.eventId 
    INNER JOIN `wp_amelia_users` 
    ON wp_amelia_events_to_providers.userId = wp_amelia_users.id 
    WHERE wp_amelia_customer_bookings.customerId = ? ');
   $req->execute(array($customerId));
   return $req->fetchAll(PDO::FETCH_ASSOC);
};

function updateInfoFromOneClient($phone, $adress, $zipcode, $city, $societe, $tva, $customerId)
{
   global $bdd;
   $req = $bdd->prepare('UPDATE wp_amelia_users SET phone = ?, adress = ?, zipcode = ?, city = ?, societe=?, tva=?  WHERE id = ?');
   $req->execute(array($phone, $adress, $zipcode, $city, $societe, $tva, $customerId));
   return $req->fetchAll(PDO::FETCH_ASSOC);
};
